package interfaceGraphique;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import classe.Professeur;


/**
 * Classe qui correspond au contenu de la fenetre professeur dans l'onglet emploi du temps dans l'interface graphique.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauEmploiDuTempsProf extends JPanel{

	/**
	 * Attribut qui correspond a un panneau déroulant qui contient les 
	 * promotions, les salles et les professeurs
	 */
	private JScrollPane defilementEDT;

	/**
	 * Attribut qui correspond a une zone de texte avec l emploi du temps selectionne par le professeur
	 */
	private JTextPane edt;

	/**
	 * Attribut qui correspond au professeur
	 */
	private Professeur prof;

	/**
	 * Constructeur de PanneauEmploiDuTempsProf
	 */
	public PanneauEmploiDuTempsProf(Professeur prof) {
		super();
		this.proprieteEmploiDuTempsProf(prof);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du 
	 * panneauEmploiDuTempsProf
	 */
	private void proprieteEmploiDuTempsProf(Professeur prof) {
		this.prof = prof;
		this.setLayout(null);
		this.setPreferredSize(new Dimension(1000,600));
		this.propEdt();
		this.propDefilementEDT();
	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonfond.png";
		String courant = new File("").getAbsolutePath();
		String path = courant+"/images" + filePath;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/**
	 * Methode permettant d initialiser toutes les proprietes du JScrollPane
	 * defilementEDT
	 */
	private void propDefilementEDT() {
		this.defilementEDT = new JScrollPane(this.edt);
		this.defilementEDT.setBounds(100,100,500,265);
		this.add(this.defilementEDT);
		//permet que le curseur de defilement commence en haut
		this.edt.grabFocus();
		this.edt.setCaretPosition(0);
	}

	/**
	 * Methode permettant d initialiser toutes les proprietes du JTextPane edt
	 */
	private void propEdt() {
		String s = this.prof.visualiserEmploiDuTempsProf();
		this.edt = new JTextPane();
		this.edt.setText(s);
		this.edt.setFont(new Font(s,Font.PLAIN,17));
		this.edt.setEditable(false);
		this.add(this.edt);
	}

}