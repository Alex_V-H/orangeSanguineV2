package interfaceGraphique;

/**
 * Classe mainInterface pour l'utilisation graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class MainInterface {
	
	public static void main(String args[]){
		FenetreDemarrage f = new FenetreDemarrage();
		f.setVisible(true);
	}
	
}