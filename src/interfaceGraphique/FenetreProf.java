package interfaceGraphique;

import java.util.List;
import javax.swing.JFrame;
import classe.Professeur;
import classe.Promotion;

/**
 * Classe désignant la fenêtre d'utilisation 
 * de l'application pour les professeurs
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class FenetreProf extends JFrame{

	/**
	 * attribut qui correspond au contenu de la fenêtre des professeurs.
	 */
	private PanneauProf pan;

	/**
	 * Constructeur de FenetreProf
	 * @param user
	 * @param promos
	 */
	public FenetreProf(Professeur user, List<Promotion> promos ) {
		super();
		this.proprieteFenetreProf(user, promos);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes de la fenetre
	 * @param user
	 * @param promos
	 */
	private void proprieteFenetreProf(Professeur user,List<Promotion> promos) {
		this.setSize(1000,600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setAlwaysOnTop(false);
		this.pan=new PanneauProf(user, promos);
		this.setContentPane(pan);
	}

}