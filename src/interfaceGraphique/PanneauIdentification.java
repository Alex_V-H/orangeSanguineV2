package interfaceGraphique;

import java.awt.Choice;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import classe.Etudiant;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;


/**
 * Classe qui correspond au contenu de la fenetre identification dans l'interface graphique.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauIdentification extends JPanel implements ActionListener{
	
	/**
	 * Attribut qui correspond a l'etiquette Nom
	 */
	private JLabel etiquetteNom;
	
	/**
	 * Attribut qui correspond a l'etiquette Mot de passe
	 */
	private JLabel etiquetteMdp;
	
	/**
	 * Attribut qui correspond a la zone de texte qui explique qu'il faut appuyer sur ENTER après la saisie d'un champs 
	 */
	private JLabel message;
	
	/**
	 * Attribut qui correspond au bouton Valider
	 */
	private JButton boutonValider;
	
	/**
	 * Attribut qui correspond au choix de personne fait dans le menu deroulant
	 */
	private Choice choix;
	
	/**
	 * Attribut qui correspond a la zone de texte pour le Nom de la personne
	 */
	private JTextField champTexteNom;
	
	/**
	 * Attribut qui correspond a la zone de texte pour le mot de passe de la personne
	 */
	private JTextField champTexteMdp;
	
	/**
	 * Attribut qui correspond a l attribut Nom sous forme de String
	 */
	private String nom;
	
	/**
	 * Attribut qui correspond a l attribut Mot de Passe sous forme de String
	 */
	private String mdp;
	
	/**
	 * Attribut qui correspond a l attribut du statut de la personne sous forme de String
	 */
	private String statut;
	
	/**
	 * Attribut qui correspond a la liste de professeurs
	 */
	private List<Professeur> professeurs;
	
	/**
	 * Attribut qui correspond a la liste d etudiants
	 */
	private List<Etudiant> etudiants;
	
	/**
	 * Attribut qui correspond a la liste de personnels administratifs
	 */
	private List<PersonnelAdministratif> admins;
	
	/**
	 * Attribut qui correspond a la liste de promotions
	 */
	private List<Promotion> promos;
	
	/**
	 * Attribut qui correspond a la liste de salles
	 */
	private List<Salle> salles;

	/**
	 * Constructeur de PanneauIdentification
	 * @param professeurs
	 * @param etudiants
	 * @param admins
	 * @param promos
	 * @param salles
	 */
	public PanneauIdentification(List<Professeur> professeurs, List<Etudiant> etudiants,
			List<PersonnelAdministratif> admins, List<Promotion> promos, List<Salle> salles) {
		super();
		this.proprietePanneauIdentification(professeurs,etudiants,admins,promos,salles);
	}

	/**
	 * Methode pour initialiser les proprietes du panneau identification.
	 * @param professeurs
	 * @param etudiants
	 * @param admins
	 * @param promos
	 * @param salles
	 */
	private void proprietePanneauIdentification(List<Professeur> professeurs, List<Etudiant> etudiants, 
			List<PersonnelAdministratif> admins, List<Promotion> promos, List<Salle> salles) {
		this.etudiants=etudiants;
		this.professeurs=professeurs;
		this.salles=salles;
		this.promos=promos;
		this.admins=admins;
		this.setLayout(null);
		this.propEtiquetteNom();
		this.propEtiquetteMdp();
		this.propBoutonValider();
		this.propChampTexteNom();
		this.propChampTexteMdp();
		this.propMessage();
		this.nom="";
		this.mdp="";
		this.propDeroulant();
	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans la fenetre.
	 */
	@Override
	protected void paintComponent(Graphics g) {
        String filePath  ="/bonfond.png";
        String courant = new File("").getAbsolutePath();
        String path = courant+"/images" + filePath;
        try {
            BufferedImage image = ImageIO.read(new File(path));
            g.drawImage(image, 0, 0, null);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    } 

	/**
	 * Methode permettant l affichage d une fenetre informative lors d une authentification rate. 
	 */
	private static void afficheRate() {
		JOptionPane.showMessageDialog(null,"Authentification rate",
				"Information",JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Methode permettant l affichage d une fenetre informative lors d une authentification reussie. 
	 */
	private static void afficheReussi() {
		JOptionPane.showMessageDialog(null,"Authentification reussie",
				"Information",JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Methode permettant l initialisation des propriete du menu deroulant.
	 */
	private void propDeroulant() {
		this.choix = new Choice();  
		this.choix.setBounds(280,170,350,20);  
		this.choix.add("Etudiant");
		this.choix.add("Professeur");  
		this.choix.add("Personnel Administratif");
		this.add(this.choix);

	}

	/**
	 * Methode permettant l initialisation de l etiquette Nom.
	 */
	private void propEtiquetteNom() {
		this.etiquetteNom=new JLabel();
		this.etiquetteNom.setBounds(300,230,350,20);
		this.etiquetteNom.setText("Nom");
		this.add(this.etiquetteNom);
	}

	/**
	 * Methode permettant l initialisation de l etiquette Mot de Passe.
	 */
	private void propEtiquetteMdp() {
		this.etiquetteMdp=new JLabel();
		this.etiquetteMdp.setBounds(300,270,350,20);
		this.etiquetteMdp.setText("Mot de passe");
		this.add(this.etiquetteMdp);
	}

	/**
	 * Methode permettant l initialisation de la zone de texte qui signale qu il faut appuyer
	 * sur ENTER apres chaque saisie.
	 */
	private void propMessage() {
		this.message=new JLabel();
		this.message.setBounds(210,420,800,50);
		this.message.setText("Il faut appuyer sur ENTER après la saisie d'un champs "
				+ "pour qu'il soit comptabilisé");
		this.message.setFont(new Font("Serif",Font.BOLD,16));
		this.message.setOpaque(false);
		this.add(this.message);
	}

	/**
	 * Methode permettant l initialisation de la zone de texte Valider
	 */
	private void propBoutonValider() {
		this.boutonValider = new JButton();
		this.boutonValider.setText("Valider");
		this.boutonValider.setBounds(450,320,100,20);
		this.add(this.boutonValider);
		this.boutonValider.addActionListener(this);

	}

	/**
	 * Methode permettant l initialisation du champs de texte pour la saisie du Nom.
	 */
	private void propChampTexteNom() {
		this.champTexteNom =new JTextField();
		this.champTexteNom.setBounds(400,230,200,20);
		this.add(this.champTexteNom);
		this.champTexteNom.addActionListener(this);
	}

	/**
	 * Methode permettant l initialisation du champs de texte pour la saisie du Mot de passe.
	 */
	private void propChampTexteMdp() {
		this.champTexteMdp =new JTextField();
		this.champTexteMdp.setBounds(400,270,200,20);
		this.add(this.champTexteMdp);
		this.champTexteMdp.addActionListener(this);
	}

	/**
	 * Methode permettant de remettre a vide les deux champs de texte
	 */
	public void saisieReinit() {
		this.champTexteNom.setText("");
		this.champTexteMdp.setText("");
	}

	/**
	 * Methode permettant de verifier si la personne identifiee existe et si son mot de passe est correct
	 * @return false si l authentification n est pas reussi et true si elle est correcte
	 */
	private boolean verificationIdentite() {
		if (this.statut.equals("Etudiant")) {
			for (Etudiant etudiant:this.etudiants) {
				if ((this.nom.equals(etudiant.getNom()))&&((this.mdp.equals(etudiant.getMdp())))){
					return true;
				}
			}
		}
		else if (this.statut.equals("Professeur")) {
			for (Professeur professeur:this.professeurs) {
				if ((this.nom.equals(professeur.getNom()))&&((this.mdp.equals(professeur.getMdp())))){
					return true;
				}
			}
		}
		else if (this.statut.equals("Personnel Administratif")){
			for (PersonnelAdministratif administrateur:this.admins) {
				if ((this.nom.equals(administrateur.getNom()))&&((this.mdp.equals(administrateur.getMdp())))){
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * Methode permettant de recuperer un prof dans la liste de professeur
	 * @return Professeur
	 */
	private Professeur recuperationProf() {
		for(Professeur prof: this.professeurs) {
			if (prof.getNom().equals(this.nom)) {
				return(prof);
			}
		}
		return (new Professeur());	
	}	

	/**
	 * Methode permettant de recuperer un etudiant dans la liste d'etudiant
	 * @return Etudiant
	 */
	private Etudiant recuperationEleve() {
		for(Etudiant eleve: this.etudiants) {
			if (eleve.getNom().equals(this.nom)) {
				return(eleve);
			}
		}
		return(new Etudiant());
	}

	/**
	 * Methode permettant de recuperer un membre du personnel administratif dans la liste du personnel administratif
	 * @return PersonnelAdministratif
	 */
	private PersonnelAdministratif recuperationAdmin() {
		for(PersonnelAdministratif admin: this.admins) {
			if (admin.getNom().equals(this.nom)) {
				return(admin);
			}
		} 
		return (new PersonnelAdministratif());
	}

	
	/**
	 *Methode permettant de gerer les actions dans le panneauIdentification
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.champTexteNom) {
			this.nom=this.champTexteNom.getText();
		}
		if(e.getSource() == this.champTexteMdp) {
			this.mdp=this.champTexteMdp.getText();
		}
		if (e.getSource() == this.boutonValider) {
			this.statut=this.choix.getItem(this.choix.getSelectedIndex());
			if (this.verificationIdentite()) {
				afficheReussi();
				saisieReinit();
				if (this.statut.equals("Professeur")){
					FenetreProf f = new FenetreProf(this.recuperationProf(), this.promos);
					f.setVisible(true);
				}
				if (this.statut.equals("Etudiant")){
					FenetreEtudiant f = new FenetreEtudiant(this.recuperationEleve());
					f.setVisible(true);
				}
				if (this.statut.equals("Personnel Administratif")){
					FenetreAdmin f = new FenetreAdmin(this.recuperationAdmin(),this.professeurs,this.promos,this.salles);
					f.setVisible(true);
				}
			}
			else {
				afficheRate();
				saisieReinit();
			}
		}
	}
	
}