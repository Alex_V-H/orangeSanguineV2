package interfaceGraphique;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Classe qui correspond au panneau de demarrage de l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauDemarrage extends JPanel{

	/**
	 * Attribut correspondant a la version de l application
	 */
	private JLabel version;

	/**
	 * Constructeur de PanneauDemarrage vide
	 */
	public PanneauDemarrage() {
		super();
		this.propPanneauDemarrage();
	}

	/**
	 * Methode permettant d initialiser le panneau de demarrage
	 */
	private void propPanneauDemarrage() {
		this.setLayout(null);
		this.propVersion();

	}


	/**
	 * Methode permettant d intialiser le composant version
	 */
	private void propVersion() {
		this.version=new JLabel();
		this.version.setText("Alpha Version 1.0");
		this.version.setBounds(135,170,200,20);
		this.version.setFont(new Font("Serif",Font.ITALIC,10));
		this.version.setOpaque(false);
		this.add(this.version);
	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonlogo.png";
		String courant = new File("").getAbsolutePath();
		String path = courant +"/images"+ filePath;

		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 
}
