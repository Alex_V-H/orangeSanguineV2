package interfaceGraphique;

import java.util.List;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

/**
 * Classe qui correspond au contenu de la fenêtre du personnel administratif dans l'interface graphique
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class PanneauAdmin extends JPanel {
	
	/**
	 * Attribut coorespondant aux onglets de la fenetre
	 */
	private JTabbedPane onglets;
	
	/**
	 * Attribut correspondant à l'onglet emploi du temps de la fenetre personnel administratif
	 */
	private PanneauEmploiDuTempsAdmin edt;
	
	/**
	 * Attribut correspondant à l'onglet de gestion des cours de la fenetre personnel administratif
	 */
	private PanneauGestionCours panneauGestionCours;

	/**
	 * Constructeur de PanneauAdmin
	 * @param user
	 * @param profs
	 * @param promos
	 * @param salles
	 */
	public PanneauAdmin(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		super();
		this.proprietePanneauAdmin(user, profs, promos,salles);
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes du panneau
	 * @param user
	 * @param profs
	 * @param promos
	 * @param salles
	 */
	private void proprietePanneauAdmin(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		this.edt=new PanneauEmploiDuTempsAdmin(user, profs, promos,salles);
		this.panneauGestionCours= new PanneauGestionCours(user,profs,promos,salles); 
		this.propOnglets();		
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes des onglets
	 */
	private void propOnglets() {
		this.onglets=new JTabbedPane(SwingConstants.TOP);
		this.onglets.addTab("Emploi du Temps", this.edt);
		this.onglets.addTab("Gestion Cours", this.panneauGestionCours);
		this.onglets.setOpaque(true);
		this.add(this.onglets);
	}

}