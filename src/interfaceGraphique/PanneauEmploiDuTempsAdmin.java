package interfaceGraphique;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import classe.Professeur;
import classe.Promotion;
import classe.Salle;
import classe.PersonnelAdministratif;


/**
 * Classe qui correspond à l'onglet Emploi du temps dans la fenêtre personnel administratif de l'interface graphique
 * Cet onglet permet la visualisation des emplois du temps des prfesseurs, etudiants et salles.
 * @author Sengeissen Enola, Alex Van Hecke
 *
 */

@SuppressWarnings("serial")
public class PanneauEmploiDuTempsAdmin extends JPanel implements ActionListener,TreeSelectionListener{

	/**
	 * Attribut qui correspond au menu déroulant pour choisir une promotion
	 */
	private JComboBox<Promotion> choixPromo;

	/**
	 * Attribut qui correspond au menu déroulant pour choisir un professeur
	 */
	private JComboBox<Professeur> choixProf;

	/**
	 * Attribut qui correspond à la liste de professeurs de l'établissement
	 */
	private List<Professeur> profs;

	/**
	 * Attribut qui correspond à la liste des promotions de l'établissemnt
	 */
	private List<Promotion> promos;

	/**
	 * Attribut qui correspond à la liste des salles de l'établissemnt
	 */
	private List<Salle> salles;

	/**
	 * Attribut qui correspond à l'employé administratif utilisateur
	 */
	private PersonnelAdministratif admin;

	/**
	 * Attribut correspondant à l'affichage de l'emploi du temps du professeur sélectionné
	 */
	private JTextPane edtProf;

	/**
	 * Attribut correspondant à l'affichage de l'emploi de la promotion sélectionnée
	 */
	private JTextPane edtPromo;

	/**
	 * Attribut correspondant à l'affichage de l'emploi de la salle sélectionnée
	 */
	private JTextPane edtSalle;

	/**
	 * Attribut correspondant à la zone d'affichage de l'emploi du temps séléctionné
	 */
	private JScrollPane defilementEDT;

	/**
	 * Attribut dictionnaire des promotions qui sert à l'initialisation de l'arbre
	 */
	private Hashtable<String,Promotion> mapPromos;

	/**
	 * Attribut dictionnaire des promotions qui sert à l'initialisation de l'arbre
	 */
	private Hashtable<String,Professeur> mapProfs;

	/**
	 * Attribut dictionnaire des salles qui sert à l'initialisation de l'arbre
	 */
	private Hashtable<String,Salle> mapSalles;

	/**
	 * Attribut qui correspond a un arbre 
	 */
	private JTree arbre;
	private JScrollPane defilementArbre;
	private Promotion promo;
	private Professeur prof;
	private Salle salle;

	/**
	 * Constructeur de PanneauEmploiDuTempsAdmin
	 * @param salles
	 */
	public PanneauEmploiDuTempsAdmin(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		super();
		this.proprieteEmploiDuTemps(user, profs, promos,salles);
	}

	/**
	 * Fonction permettant d initialiser toutes les proprietes du
	 * PanneauEmploiDuTempsAdmin
	 * @param salles
	 */
	private void proprieteEmploiDuTemps(PersonnelAdministratif user, List<Professeur> profs, List<Promotion> promos, List<Salle> salles) {
		this.setLayout(null);
		this.setPreferredSize(new Dimension(1000,600));
		this.mapPromos = new Hashtable<String,Promotion>();
		this.mapProfs = new Hashtable<String,Professeur>();
		this.mapSalles = new Hashtable<String,Salle>();
		this.profs = profs;
		this.admin = user;
		this.promos = promos;
		this.salles=salles;
		this.promo=new Promotion();
		this.prof=new Professeur();
		this.salle=new Salle();
		this.propMapPromos();
		this.propMapProfs();
		this.propMapSalles();
		this.propArbre();
		this.propDefilementArbre();
		this.propEdtProf();
		this.propEdtPromo();
		this.propEdtSalle();
		this.propDefilementEDT(new JTextPane());

	}

	/**
	 * Methode qui permet de mettre une image en arriere plan dans le panneau.
	 */
	protected void paintComponent(Graphics g) {
		String filePath  ="/bonfond.png";
		String courant = new File("").getAbsolutePath();
		String path = courant+"/images" + filePath;
		try {
			BufferedImage image = ImageIO.read(new File(path));
			g.drawImage(image, 0, 0, null);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	} 

	/**
	 * Fonction permettant d initialiser toutes les proprietes du JScrollPane
	 * defilementArbre
	 */
	private void propDefilementArbre() {
		defilementArbre = new JScrollPane(this.arbre);
		this.defilementArbre.setBounds(5,5,200,590);
		this.add(this.defilementArbre);

	}

	/**
	 * Methode permettant d initialiser la map de promotions
	 */
	private void propMapPromos() {
		for (int i=0;i<this.promos.size();i++) {
			this.mapPromos.put(this.promos.get(i).getId(),this.promos.get(i));
		}
	}

	/**
	 * Methode permettant d initialiser la mapde professeurs
	 */
	private void propMapProfs() {
		for (int i=0;i<this.profs.size();i++) {
			this.mapProfs.put(this.profs.get(i).toString(),this.profs.get(i));
		}
	}

	/**
	 * Methode permettant d initialiser la mapde salles
	 */
	private void propMapSalles() {
		for (int i=0;i<this.salles.size();i++) {
			this.mapSalles.put(this.salles.get(i).getNom(),this.salles.get(i));
		}
	}


	/**
	 * Methode permettant d initialiser l arbre contenant les promotions, les professeurs
	 * ainis que les salles
	 */
	private void propArbre() {
		DefaultMutableTreeNode top =new DefaultMutableTreeNode("Etablissement");
		DefaultMutableTreeNode category = null;
		DefaultMutableTreeNode category2 = null;
		DefaultMutableTreeNode category3 = null;
		DefaultMutableTreeNode element = null;
		Set<String> cles =this.mapPromos.keySet();
		Iterator<String> it = cles.iterator();
		while (it.hasNext()) {
			String cle = it.next();
			if (category==null) {
				category = new DefaultMutableTreeNode(this.mapPromos.get(cle).getClass().getName().substring(7)+"s");
			}
			element = new DefaultMutableTreeNode(this.mapPromos.get(cle).getId());
			category.add(element);
		}
		top.add(category);
		Set<String> cles2 =this.mapProfs.keySet();
		Iterator<String> it2 = cles2.iterator();

		while (it2.hasNext()) {
			String cle2 = it2.next();
			if (category2==null) {
				category2 = new DefaultMutableTreeNode(this.mapProfs.get(cle2).getClass().getName().substring(7)+"s");
			}
			element = new DefaultMutableTreeNode(this.mapProfs.get(cle2).toString());
			category2.add(element);

		}
		top.add(category2);
		this.arbre= new JTree(top);
		this.arbre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.arbre.addTreeSelectionListener(this);
		this.arbre.setRootVisible(true);
		this.add(this.arbre);

		Set<String> cles3 =this.mapSalles.keySet();
		Iterator<String> it3 = cles3.iterator();

		while (it3.hasNext()) {
			String cle3 = it3.next();
			if (category3==null) {
				category3 = new DefaultMutableTreeNode("Salles");
			}
			element = new DefaultMutableTreeNode(this.mapSalles.get(cle3).getNom());
			category3.add(element);

		}
		top.add(category3);
		this.arbre= new JTree(top);
		this.arbre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.arbre.addTreeSelectionListener(this);
		this.arbre.setRootVisible(true);
		this.add(this.arbre);

	}

	/**
	 * Methode permettant d initialiser le texte de l emploi du temps de la promo
	 */
	private void propEdtProf() {
		String s = this.admin.visualiserEmploiDuTempsProf(prof);
		this.edtProf=new JTextPane();
		this.edtProf.setText(s);
		this.edtProf.setFont(new Font(s, Font.PLAIN,17));
		this.edtProf.setEditable(false);
		this.add(this.edtProf);  
	}

	/**
	 * Methode permettant d initialiser le texte de l emploi du temps de la promo
	 */
	private void propEdtPromo() {
		String s = this.admin.visualiserEmploiDuTempsPromo(promo);
		this.edtPromo=new JTextPane();
		this.edtPromo.setText(s);
		this.edtPromo.setFont(new Font(s, Font.PLAIN,17));
		this.edtPromo.setEditable(false);
		this.add(this.edtPromo);  
	}


	/**
	 * Methode permettant d initialiser le texte de l emploi du temps de la salle
	 */
	private void propEdtSalle() {
		String s = this.admin.visualiserEmploiDuTempsSalle(salle);
		this.edtSalle=new JTextPane();
		this.edtSalle.setText(s);
		this.edtSalle.setFont(new Font(s, Font.PLAIN,15));
		this.edtSalle.setEditable(false);
		this.add(this.edtSalle);  
	}

	/**
	 * Methode permettant d initialiser le panneau deroulant de l emploi du temps
	 * @param edt
	 */
	private void propDefilementEDT(JTextPane edt) {
		this.defilementEDT = new JScrollPane(edt);
		this.defilementEDT.setBounds(230,60,450,480);
		this.add(this.defilementEDT);
		//permet que le curseur de defilement commence en haut
		edt.grabFocus();
		edt.setCaretPosition(0);
	}


	/**
	 * Methode permettant de gerer les actions dans le panneau
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==this.choixProf) {
			this.propEdtProf();
		}

		if(e.getSource()==this.choixPromo) {
			this.propEdtPromo();
		}
	}


	/**
	 * Methode permettant de trouver une promotion dans une liste a partir d une chaine
	 * de caractere passee en argument correspondant a son id
	 * @param s
	 */
	private void recupPromo(String[] s) {
		for (int i=0;i<this.promos.size();i++) {
			if (this.promos.get(i).getId().equals(s[0])) {
				this.promo=this.promos.get(i);
			}
		}
	}


	/**
	 * Methode permettant de trouver un professeur dans une liste a partir d une chaine
	 * de caractere passee en argument correspondant a son nom
	 * @param s
	 */
	private void recupProf(String[] s) {
		for (int i=0;i<this.profs.size();i++) {
			if ((this.profs.get(i).getNom().equals(s[0])) && (this.profs.get(i).getPrenom().equals(s[1]))) {
				this.prof=this.profs.get(i);
			}
		}
	}

	/**
	 * Methode permettant de trouver une salle dans une liste a partir d une chaine
	 * de caractere passee en argument correspondant a son nom
	 * @param s
	 */
	private void recupSalle(String[] s) {
		for (int i=0;i<this.salles.size();i++) {
			if (this.salles.get(i).getNom().equals(s[0])) {
				this.salle=this.salles.get(i);
			}
		}
	}


	/**
	 * Methode permettant de gerer les actions dans le panneau
	 */
	@Override
	public void valueChanged(TreeSelectionEvent e) {
		TreePath t=e.getPath();
		String b = t.toString().substring(1,t.toString().length()-1);
		if (e.getSource()==this.arbre) {
			String[] c = b.split(", ");
			if (c.length==3) {
				String[] d = c[2].split(" ");
				if (c[1].equals("Promotions")) {
					this.remove(edtPromo);
					this.remove(defilementEDT);
					this.recupPromo(d);
					this.propEdtPromo();
					this.propDefilementEDT(edtPromo);
					this.updateUI();
				}
				if (c[1].equals("Professeurs")) {
					this.remove(edtProf);
					this.remove(defilementEDT);
					this.recupProf(d);
					this.propEdtProf();
					this.propDefilementEDT(edtProf);
					this.updateUI();
				}
				if (c[1].equals("Salles")) {
					this.remove(edtSalle);
					this.remove(defilementEDT);
					this.recupSalle(d);
					this.propEdtSalle();
					this.propDefilementEDT(edtSalle);
					this.updateUI();
				}
			}
		}
	}
}

