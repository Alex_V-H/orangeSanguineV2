package interfaceGraphique;

import java.util.List;
import javax.swing.JFrame;
import classe.Etudiant;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

/**
 * Classe désignant la fenêtre d'identification
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
@SuppressWarnings("serial")
public class FenetreIdentification extends JFrame{

	/**
	 * attribut qui correspond au contenu de la fenêtre d'identification.
	 */
	private PanneauIdentification pan;

	/**
	 * Constructeur de FenetreIdentification
	 * @param salles 
	 * @param promos 
	 * @param admins 
	 * @param etudiants 
	 * @param professeurs 
	 */
	public FenetreIdentification(List<Professeur> professeurs, List<Etudiant> etudiants, List<PersonnelAdministratif> admins, List<Promotion> promos, List<Salle> salles) {
		super();
		this.proprieteMyFrame(professeurs,etudiants,admins,promos,salles);	
	}

	/**
	 * Méthode permettant d initialiser toutes les proprietes de la fenetre
	 * @param salles 
	 * @param promos 
	 * @param admins 
	 * @param etudiants 
	 * @param professeurs 
	 */
	private void proprieteMyFrame(List<Professeur> professeurs, List<Etudiant> etudiants, List<PersonnelAdministratif> admins, List<Promotion> promos, List<Salle> salles) {
		this.setSize(1000,600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setAlwaysOnTop(false);
		this.pan=new PanneauIdentification(professeurs,etudiants,admins,promos,salles);
		this.setContentPane(pan);
	}
}