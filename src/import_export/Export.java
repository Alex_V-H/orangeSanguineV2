package import_export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classe.Etudiant;
import classe.Matiere;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Salle;

public class Export {


	public static void serialiserEtudiants(List<Etudiant> etuds) {
		ObjectOutputStream oos =null;
		try {
			final FileOutputStream fichier = new FileOutputStream("Etudiant.ser");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(etuds);
			oos.flush();
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void serialiserProfesseurs(List<Professeur> profs) {
		ObjectOutputStream oos =null;
		try {
			final FileOutputStream fichier = new FileOutputStream("Professeurs.ser");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(profs);
			oos.flush();
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}


	public static void serialiserSalles(List<Salle> salles) {
		ObjectOutputStream oos =null;
		try {
			final FileOutputStream fichier = new FileOutputStream("Salles.ser");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(salles);
			oos.flush();
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}


	public static void serialiserPersonnelAdmin(List<PersonnelAdministratif> admins) {
		ObjectOutputStream oos =null;
		try {
			final FileOutputStream fichier = new FileOutputStream("PersonnelsAdministratif.ser");
			oos = new ObjectOutputStream(fichier);
			oos.writeObject(admins);
			oos.flush();
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static void sortieBulletin(Etudiant etud) {
		String bulletin = etud.toStringBulletin();
		Calendar today = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
		String strDate = dateFormat.format(today.getTime());
		String courant =new File("").getAbsolutePath(); 
		String path = courant + "/bulletins/"+"Bulletin" + etud.getNom()+".txt";
		Path p= Paths.get(path);
		if (!Files.exists(p)) {
			try {
				Files.createFile(p);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		else { 
			File file = new File(path);
			file.delete();
			try {
				Files.createFile(p);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try (BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8,
				StandardOpenOption.WRITE)) {
			writer.write("Bulletin de : " + etud.getNom() + " " + etud.getPrenom()
			+ "\n\nEdité le : "+ strDate +"\n\nMatière \t\tMoyenne\n\n");
			writer.write(bulletin);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}

