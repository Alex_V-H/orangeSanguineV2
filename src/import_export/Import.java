package import_export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import classe.Equipement;
import classe.Etudiant;
import classe.Matiere;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

public class Import {

	public Import() {

	}


	//	/**
	//	 * Methode permettant d initialiser les professeurs par la lecture d un fichier
	//	 */
	//	private void importProfs() {
	//		String filePath  ="/Professeurs.txt";
	//		String courant =new File("").getAbsolutePath();
	//		String path = courant +"/donnees"+ filePath;
	//		Path p= Paths.get(path);
	//		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
	//			String lines;
	//			while ((lines = reader.readLine()) != null) {
	//				String[] line = lines.split(" ; ");
	//				for (Matiere m : Matiere.values()) {
	//					if (m.toString().equals(line[3])){
	//						Professeur prof = new Professeur(line[0], line[1],m,line[2]);
	//						this.professeurs.add(prof);
	//					}
	//				}
	//			}
	//			reader.close();
	//		}
	//		catch (IOException e2) {
	//			e2.printStackTrace();
	//		}
	//	}
	//	
	//	
	//
	//	/**
	//	 * Methode permettant d initialiser les etudiants et les promos par la lecture d un fichier
	//	 */
	//	private void importEtudiants() {
	//		HashMap<String,List<Etudiant>> dico = new HashMap<String,List<Etudiant>>();
	//		String filePath  ="/Etudiants.txt";
	//		String courant =new File("").getAbsolutePath();
	//		String path = courant +"/donnees"+ filePath;
	//		Path p= Paths.get(path);
	//		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
	//			String lines;
	//			while ((lines = reader.readLine()) != null) {
	//				String[] line = lines.split(" ; ");
	//				if (line.length>=2) {
	//					Etudiant etud = new Etudiant(line[0], line[1],line[2]);
	//					if (dico.containsKey(line[3])) {
	//						dico.get(line[3]).add(etud);
	//					}
	//					else {
	//						ArrayList<Etudiant> aux = new ArrayList<Etudiant>();
	//						aux.add(etud);
	//						dico.put(line[3],aux);
	//					}
	//					etudiants.add(etud);
	//				}
	//				else {
	//				}
	//			}
	//		}
	//		catch (IOException e2) {
	//			e2.printStackTrace();
	//		}
	//	}
	//	
	//	
	//	/**
	//	 * Methode permettant d initialiser le personel administrateur par la lecture d un fichier
	//	 */
	//	private void initAdmins() {
	//		String filePath  ="/PersonnelAdmin.txt";
	//		String courant =new File("").getAbsolutePath();
	//		String path = courant +"/donnees"+ filePath;
	//		Path p= Paths.get(path);
	//		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
	//			String lines;
	//			while ((lines = reader.readLine()) != null) {
	//				String[] line = lines.split(" ; ");
	//				PersonnelAdministratif pa = new PersonnelAdministratif(line[0], line[1],line[2]);
	//				this.personnelsAdministratif.add(pa);
	//			}
	//			reader.close();
	//		}
	//		catch (IOException e2) {
	//
	//			e2.printStackTrace();
	//		}
	//	}
	//	
	//	/**
	//	 * Methode permettant d initialiser les salles par la lecture d un fichier
	//	 */
	//	private void initSalles() {
	//		String filePath  ="/Salles.txt";
	//		String courant =new File("").getAbsolutePath();
	//		String path = courant +"/donnees"+ filePath;
	//		Path p= Paths.get(path);
	//		try (BufferedReader reader = Files.newBufferedReader(p, StandardCharsets.UTF_8)) {
	//			String lines;
	//			while ((lines = reader.readLine()) != null) {
	//				String[] line = lines.split(" ; ");
	//				if (line.length>2) {
	//					HashMap<Equipement,Integer> mapEquip = new HashMap<Equipement,Integer>();
	//					for (int i=2;i<line.length && i<line.length-1;i+=2) {
	//						for(Equipement equip : Equipement.values()) {
	//							if (equip.toString().contentEquals(line[i])) {
	//								mapEquip.put(equip,Integer.parseInt(line[i+1]));
	//							}
	//						}
	//					}
	//					Salle salle = new Salle(Integer.parseInt(line[1]), line[0],mapEquip);
	//					this.salles.add(salle);
	//				}
	//
	//			}
	//			reader.close();
	//		}
	//		catch (IOException e2) {
	//			e2.printStackTrace();
	//		}
	//	}


	@SuppressWarnings("unchecked")
	public static List<Etudiant> deserialiserEtudiants() throws ClassNotFoundException {
		ObjectInputStream ois =null;
		try {
			final FileInputStream fichier = new FileInputStream("Etudiant.ser");
			ois = new ObjectInputStream(fichier);
			final List<Etudiant> etuds = (List<Etudiant>) ois.readObject();
			return etuds;
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (ois != null) {
					ois.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		return new ArrayList<Etudiant>();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Professeur> deserialiserProfesseurs() throws ClassNotFoundException {
		ObjectInputStream ois =null;
		try {
			final FileInputStream fichier = new FileInputStream("Etudiant.ser");
			ois = new ObjectInputStream(fichier);
			final List<Professeur> profs = (List<Professeur>) ois.readObject();
			return profs;
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (ois != null) {
					ois.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		return new ArrayList<Professeur>();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Salle> deserialiserSalles() throws ClassNotFoundException {
		ObjectInputStream ois =null;
		try {
			final FileInputStream fichier = new FileInputStream("Salles.ser");
			ois = new ObjectInputStream(fichier);
			final List<Salle> salles = (List<Salle>) ois.readObject();
			return salles;
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (ois != null) {
					ois.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		return new ArrayList<Salle>();
	}
	
	
	@SuppressWarnings("unchecked")
	public static List<PersonnelAdministratif> deserialiserPersonnelsAdministratif() throws ClassNotFoundException {
		ObjectInputStream ois =null;
		try {
			final FileInputStream fichier = new FileInputStream("PersonnelsAdministratif.ser");
			ois = new ObjectInputStream(fichier);
			final List<PersonnelAdministratif> admins = (List<PersonnelAdministratif>) ois.readObject();
			return admins;
		}catch (final IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (ois != null) {
					ois.close();
				}
			}catch (final IOException ex) {
				ex.printStackTrace();
			}
		}
		return new ArrayList<PersonnelAdministratif>();
	}
}
