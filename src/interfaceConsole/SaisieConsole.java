package interfaceConsole;

import java.util.Scanner;

import classe.Etudiant;
import classe.Professeur;
import classe.Promotion;


//Utilite de la classe ???
public class SaisieConsole {
	
	private String id;
	
	private String mdp;
	
	private String dateDebut;
	
	private String dateFin;
	
	private String dateChoisie;
	
	private String matiere;
	
	private Professeur prof;
	
	private Etudiant etud;
	
	private Promotion promo;
	
	
	public void saisieIdentification() {
		System.out.println("Veuillez saisir votre identifiant");
		Scanner sc = new Scanner(System.in);
		this.setId(sc.nextLine());
		sc.close();
	}
	
	public void saisieMdp() {
		System.out.println("Veuillez saisir votre mot de passe");
		Scanner sc = new Scanner(System.in);
		this.setMdp(sc.nextLine());
		sc.close();
	}
	
	public String getId() {
		return id;
	}

	public String getMdp() {
		return mdp;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}



}
