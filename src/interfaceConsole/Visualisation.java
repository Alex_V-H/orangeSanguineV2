package interfaceConsole;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classe.Calendrier;
import classe.Etudiant;
import classe.Matiere;
import classe.Note;
import classe.PersonnelAdministratif;
import classe.Professeur;
import classe.Promotion;
import classe.Salle;

import import_export.Export;
import import_export.Import;

public class Visualisation {

	public static void visualiserEdT(Calendrier cal, String debut, String fin, int p) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss"); 
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		Date date_debut= dateFormat.parse(debut);
		Date date_fin= dateFormat.parse(fin);
		start.setTime(date_debut);
		end.setTime(date_fin);
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(start)<0) {
			c++;
			//on recupere l indice qui correspond a celui de la date d aujourd hui
			//dans le calendrier de la salle pour pouvoir commencer
			//l affichage de l emploi du temps qu a partir d aujourd hui.
		}
		while(c<cal.getEmploiDuTemps().size()&& (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(end)<0) {
			String strDate = dateFormat.format(cal.getEmploiDuTemps().get(c).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(c).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				System.out.println("\n\t" + strDate.substring(0,10) + "\n\n");
			}
			System.out.println(strDate.substring(11) + "\t");
			if (cal.getEmploiDuTemps().get(c).getCours()!=null) {
				//promo
				if (p == 0) {
					System.out.println(cal.getEmploiDuTemps().get(c).getCours().getSalle().getNom()+" , " +
					cal.getEmploiDuTemps().get(c).getCours().getProf().getPrenom() + " " +
					cal.getEmploiDuTemps().get(c).getCours().getProf().getNom() + " , "
					+ cal.getEmploiDuTemps().get(c).getCours().getMatiere());
				}
				//professeur
				else if (p == 1) {
					System.out.println(cal.getEmploiDuTemps().get(c).getCours().getSalle().getNom()+" , " +
					cal.getEmploiDuTemps().get(c).getCours().getPromo().getId() + " , "
					+ cal.getEmploiDuTemps().get(c).getCours().getMatiere());
				}
				//salle
				else {
					System.out.println(cal.getEmploiDuTemps().get(c).getCours().getPromo()+" , " +
					cal.getEmploiDuTemps().get(c).getCours().getProf().getPrenom() + " " +
					cal.getEmploiDuTemps().get(c).getCours().getProf().getNom() +" , "
					+ cal.getEmploiDuTemps().get(c).getCours().getMatiere());
				}
				
			}
			else {
				System.out.println("--");
			}
			System.out.println("\n");
			c++;
		}
	}
	
	
	public static void visualiserBulletin(Etudiant e) {
		Set<Matiere> cles = e.getNotes().keySet();
		Iterator<Matiere> it = cles.iterator();
		Map<Matiere,Double> moyMat = e.calculMoyMatiere();
		System.out.println("\nEtudiant "+ e.toString() +"\n");
		while(it.hasNext()) {
			Matiere cle =it.next();
			System.out.println("Matiere : " + cle);
			for (Note note : e.getNotes().get(cle)) {
				System.out.println("note : " + note.getValeur()+ "\t" + 
						"coeff : " + note.getCoeff() + "\t");
			}
			System.out.println("Moyenne : " + (double)Math.round(moyMat.get(cle) * 10) / 10 + "\n");
		}	
		System.out.println("Moyenne generale : " + (double)Math.round(e.calculMoyGenerale() *10) / 10);
	}
	
	
	
	public static void main(String args[]) throws ParseException, ClassNotFoundException {
		Salle s = new Salle(10,"bla");
		Professeur p = new Professeur("Pintes", "Antoine", Matiere.FRANCAIS);
		Etudiant etud = new Etudiant("Van Hecke", "Alex");
		Etudiant etud2 = new Etudiant("Sobreira", "Oriane","1234");
		Note note = new Note(etud, Matiere.FRANCAIS, 12, 2);
		List<Etudiant> le = new ArrayList<Etudiant>();
		le.add(etud);
		le.add(etud2);
		Promotion pr = new Promotion(le,"1");
		etud.setPromo(pr);
		PersonnelAdministratif pa = new PersonnelAdministratif("Morand", "Myriam");
		pa.ajouterCoursGraphique("05-07-2019-09:00:00", s, p, pr, Matiere.FRANCAIS);
//		visualiserEdT(pr.getCalendrier(),"05-07-2019-08:00:00","06-07-2019-08:00:00",0);
//		visualiserEdT(s.getCalendrier(),"05-07-2019-08:00:00","06-07-2019-08:00:00",0);
//		visualiserEdT(p.getCalendrier(),"05-07-2019-08:00:00","06-07-2019-08:00:00",0);
		p.noter(note);
		Export.sortieBulletin(etud);
		Export.serialiserEtudiants(le);
		List<Etudiant> e = Import.deserialiserEtudiants();
		System.out.println(e);
		System.out.println(e.get(0).getPromo().getId());
	}
	
}
