package classe;

import java.util.Calendar;


/**
 * Classe representant un creneau.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Creneau{
	/**
	 * Attribut qui correspond a la date de debut du creneau
	 */
	private Calendar dateDebut;
	
	/**
	 * Attribut qui correspond au cours attribue au creneau
	 */
	private Cours cours;


	/**
	 * Constructeur de creneau avec une date de debut du creneau
	 * @param dateDebut
	 * @param cours
	 */
	public Creneau(Calendar calendar) {
		this.dateDebut = calendar;
		this.cours = null;
	}

	/**
	 * Constructeur de creneau avec une date de debut du creneau
	 * et son cours.
	 * @param dateDebut
	 * @param cours
	 */
	public Creneau(Calendar calendar, Cours cours) {
		this.dateDebut = calendar;
		this.cours = cours;
	}

	/**
	 * getter de la date de debut
	 * @return dateDebut
	 */
	public Calendar getDateDebut() {
		return this.dateDebut;
	}

	/**
	 * getter de cours
	 * @return cours
	 */
	public Cours getCours() {
		return this.cours;
	}

	/**
	 * setter de la date de but
	 * @param dateDebut
	 */
	public void setDateDebut(Calendar calendar) {
		this.dateDebut = calendar;
	}

	/**
	 * setter du cours
	 * @param cours
	 */
	public void setCours(Cours cours) {
		this.cours = cours;
	}

	/**
	 * Methode permettant de savoir si deux creneaux sont egaux.
	 * @param c
	 * @return (boolean) s'il s'agit oui ou non, du meme creneau 
	 */
	public boolean equals(Creneau c) {
		if (this.cours==null || c.cours==null) {
			return false;
		}
		if (this.cours.equals(c.cours) && this.dateDebut.getTime().compareTo(c.dateDebut.getTime())==0) {
			return true;
		}
		return false;
	}

}
