package classe;

/**
 * Enumeration representant un equipement.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public enum Equipement {
	VIDEOPROJECTEUR("VIDEOPROJECTEUR"),
	LOGICIEL("LOGICIEL"),
	ORDINATEUR("ORDINATEUR"),
	LAVABO("ORDINATEUR"),
	TABLEAU("TABLEAU");
	
	/**
	 * Attribut correspondant au nom de l equipement
	 */
	private String name;
	
	/**
	 * Constructeur d equipement avec un String correspondant a 
	 * son nom en parametre
	 * @param name
	 */
	Equipement(String name){
		this.name=name;
	}
	
	/**
	 * Redefinition de la methode toString pour Equipement
	 */
	@Override
	public String toString() {
		return this.name;
	}
}
