package classe;

import java.util.Calendar;
import java.util.List;

/**
 * Interface utilisée pour connaitre la disponibilite d une instance.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public interface IDisponible {
	
	/**
	 * Methode abstraite qui permet de savoir les creneaux disponibles
	 * a partir d un calendar.
	 * @param calendar
	 * @return la liste des creneaux disponibles
	 */
	public abstract List<Creneau> disponible(Calendar calendar);
}
