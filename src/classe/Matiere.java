package classe;


/**
 * Enumeration qui correspond aux matières enseignées dans l etablissement.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public enum Matiere {
	MATHEMATIQUES("MATHEMATIQUES"),
	PHYSIQUE("PHYSIQUE"),
	CHIMIE("CHIMIE"),
	BIOLOGIE("BIOLOGIE"),
	FRANCAIS("FRANCAIS"),
	ANGLAIS("ANGLAIS"),
	ESPAGNOL("ESPAGNOL"),
	ALLEMAND("ALLEMAND"),
	HISTOIRE("HISTOIRE"),
	GEOGRAPHIE("GEOGRAPHIE");

	/**
	 * Attribut qui correspond a la matiere sous forme de String
	 */
	private String name;

	/**
	 * Constructeur de Matiere
	 * @param name
	 */
	Matiere(String name){
		this.name = name;
	}

	/**
	 * Redefinition de la methode toString pour Matiere.
	 */
	@Override
	public String toString() {
		return this.name;
	}

}