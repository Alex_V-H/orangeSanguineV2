package classe;


import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Classe representant un etudiant.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Etudiant extends Personne implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Attribut qui correspond a la promotion de l etudiant
	 */
	private Promotion promo;
	
	/**
	 * Attribut qui correspond a toutes les notes de l etudiant
	 */
	private transient Map<Matiere,List<Note>> notes;

	/**
	 * Constructeur etudiant avec son nom et son prenom
	 * @param nom
	 * @param prenom
	 */
	public Etudiant(String nom, String prenom) {
		super(nom, prenom);
		this.promo=new Promotion();
		this.notes = new HashMap<Matiere,List<Note>>();
	}
	
	/**
	 * Constructeur etudiant avec son nom et son prenom et son mot de passe
	 * @param nom
	 * @param prenom
	 * @param mdp
	 */
	public Etudiant(String nom, String prenom,String mdp) {
		super(nom, prenom, mdp);
		this.promo=new Promotion();
		this.notes = new HashMap<Matiere,List<Note>>();
	}

	/**
	 * Constructeur vide d'etudiant 
	 * 
	 */
	public Etudiant() {
		super();
		this.promo=new Promotion();
		this.notes = new HashMap<Matiere,List<Note>>();
	}
	
	/**
	 * Constructeur etudiant avec son nom, son prenom, sa promotion, ses notes
	 * @param nom
	 * @param prenom
	 * @param promo
	 * @param notes
	 */
	public Etudiant(String nom, String prenom, Promotion promo, Map<Matiere, List<Note>> notes) {
		super(nom, prenom);
		this.promo = promo;
		this.notes = notes;
	}

	/**
	 * getter de la promotion
	 * @return
	 */
	public Promotion getPromo() {
		return this.promo;
	}

	/**
	 * getter de la liste de note
	 * @return
	 */
	public Map<Matiere, List<Note>> getNotes() {
		return this.notes;
	}

	/**
	 * setter de la promotion
	 * @param promo
	 */
	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	/**
	 * setter de la liste de note
	 * @param notes
	 */
	public void setNotes(Map<Matiere, List<Note>> notes) {
		this.notes = notes;
	}


	/**
	 * Methode permettant d afficher le bulletin de notes de l etudiant
	 * pour que l'etudiant puisse le visualiser.
	 * @return le contenu du bulletin sous forme de String
	 */
	public String toStringBulletin() {
		Set<Matiere> cles = this.notes.keySet();
		Iterator<Matiere> it = cles.iterator();
		Map<Matiere,Double> moyMat = calculMoyMatiere();
		String b ="";
		b+=("Etudiant "+ this.getNom() + " " + this.getPrenom()+"\n\n");
		while(it.hasNext()) {
			Matiere cle =it.next();
			b+=("Matiere : " + cle+"\n");
			for (Note note : this.notes.get(cle)) {
				b+=("note : " + note.getValeur()+ "\t" + 
						"coeff : " + note.getCoeff() + "\t"+"\n");
			}
			b+=("Moyenne : " + (double)Math.round(moyMat.get(cle) * 10) / 10 + "\n\n");
		}
		b+=("Moyenne generale : " + (double)Math.round(this.calculMoyGenerale() *10) / 10);
		return(b);
	}

	/**
	 * Methode qui calcule la moyenne de l etudiant dans chaque matiere
	 * @return un dictionnaire avec les matieres en clé
	 * et les moyennes correspondantes en valeur.
	 */
	public Map<Matiere,Double> calculMoyMatiere() {
		Map<Matiere,Double> map = new HashMap<Matiere,Double>();
		Set<Matiere> cles = this.notes.keySet();
		Iterator<Matiere> it = cles.iterator();
		while(it.hasNext()) {
			int c=0;
			double somme=0;
			Matiere cle = it.next();
			for(Note note : this.notes.get(cle)) {
				somme=somme + note.getValeur()*note.getCoeff();
				c+=note.getCoeff();
			}
			map.put(cle,somme/c);
		}
		return map;
	}


	/**
	 * Methode qui calcule la moyenne generale de l etudiant
	 * @return un réel qui est la moyenne general de l etudiant
	 */
	public double calculMoyGenerale() {
		Map<Matiere,Double> map = calculMoyMatiere();
		Set<Matiere> cles = map.keySet();
		Iterator<Matiere> it = cles.iterator();
		double somme =0;
		int c = 0;
		while (it.hasNext()) {
			Matiere cle=it.next();
			somme += map.get(cle);
			c+=1;
		}
		return (somme/c);
	}


	/**
	 * Methode qui permet de voir s'il s'agit du même etudiant.
	 * @param etudiant
	 * @return true (s'il s'agit du meme etudiant) ou false (si non).
	 */
	public boolean compareEtudiant(Etudiant etudiant) {
		return ((this.getPrenom() == etudiant.getPrenom()) &&
				(this.getNom() == etudiant.getNom()));
	}

	/**
	 * Methode qui permet l'edition du bulletin de l etudiant dans un fichier texte.
	 */
	public void editerBulletin() {
		Calendar today = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
		String strDate = dateFormat.format(today.getTime());
		String courant =new File("").getAbsolutePath(); 
		String path = courant + "/bulletins/"+"Bulletin" + this.getNom()+".txt";
		Path p= Paths.get(path);
		if (!Files.exists(p)) {
			try {
				Files.createFile(p);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		else { 
			File file = new File(path);
			file.delete();
			try {
				Files.createFile(p);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try (BufferedWriter writer = Files.newBufferedWriter(p, StandardCharsets.UTF_8,
				StandardOpenOption.WRITE)) {
			writer.write("Bulletin de : " + this.getNom() + " " + this.getPrenom()
			+ "\n\nEdité le : "+ strDate +"\n\nMatière \t\tMoyenne\n\n");
			Set<Matiere> cles = this.notes.keySet();
			Iterator<Matiere> it = cles.iterator();
			Map<Matiere,Double> moyMat = calculMoyMatiere();
			while(it.hasNext()) {

				Matiere cle =it.next();
				if (cle==Matiere.CHIMIE) {
					writer.write(cle + " \t\t\t" + (double)Math.round(moyMat.get(cle) * 10) / 10 +"\n");

				}
				else {
					writer.write(cle + " \t\t" + (double)Math.round(moyMat.get(cle) * 10) / 10 +"\n");         
				}
			}
			writer.write("\nMoyenne générale : " + (double)Math.round(this.calculMoyGenerale() *10) / 10 );
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}


	}

	/**
	 * Methode pour permettre une visualisation dans le terminal de l'emploi du temps de l etudiant.
	 * @return le contenu de ce qui est visualiser dans le terminal sous forme de String
	 */
	public String visualiserEmploiDuTemps() {
		Calendar today = Calendar.getInstance();	
		Calendrier cal = this.promo.getCalendrier();
		int c = 0;
		System.out.println("\n\nEmploi du temps de l'étudiant.e : " + this.getPrenom() + " " + this.getNom() + " (promo : " + this.promo.getId() +")\n");
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(today)<0) {
			c++;
			//on recupere l indice qui correspond a celui de la date d aujourd hui
			//dans le calendrier de la promotion pour pouvoir commencer
			//l affichage de l emploi du temps qu a partir d aujourd hui.
		}
		String e = "\tAujourd'hui\n\n";
		for (int i = c; i<cal.getEmploiDuTemps().size() && i<c+70 ;i++) {
			String s =""; 
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate2 = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\n\t" + strDate2.substring(0,10) + "\n\n";
			}
			s+= strDate2.substring(11) + "\t";
			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+=cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getPrenom() + " " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getNom() + " , "
						+ cal.getEmploiDuTemps().get(i).getCours().getMatiere();
			}
			else {
				s+="--";
			}
			e+=s+"\n";
			System.out.println(s);
		}
		return(e);
	}

}
