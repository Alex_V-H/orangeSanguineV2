package classe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Classe mainConsole 
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class MainConsole {

	public static void main(String[] args) {
		
		//Initialisation des professeurs
		Professeur prof1 = new Professeur("Costes","Benoit", Matiere.MATHEMATIQUES);
		Professeur prof2 = new Professeur("Hangouet","Jean-Francois", Matiere.FRANCAIS);
		Professeur prof3 = new Professeur("Ennafi","Oussama", Matiere.CHIMIE);
		Professeur prof5 = new Professeur("Coindet","Victor", Matiere.BIOLOGIE);

		//Initialisation des etudiants
		Etudiant etud1 = new Etudiant("VanHecke","Alex");
		Etudiant etud2 = new Etudiant("Sobreira","Oriane");
		Etudiant etud3 = new Etudiant("Sengeissen","Enola");
		Etudiant etud4 = new Etudiant("Decourt","Victor");
		Etudiant etud5 = new Etudiant("Chassard","Axel");

		List<Etudiant> etudiants1 = new ArrayList<Etudiant>();
		etudiants1.add(etud1);
		etudiants1.add(etud2);
		etudiants1.add(etud3);
		List<Etudiant> etudiants2 = new ArrayList<Etudiant>();
		etudiants2.add(etud4);
		etudiants2.add(etud5);

		//Initialisation des promotions
		Promotion promo1 = new Promotion(etudiants1,"IT1");
		Promotion promo2 = new Promotion(etudiants2,"IT2");

		etud1.setPromo(promo1);
		etud2.setPromo(promo1);
		etud3.setPromo(promo1);
		
		etud4.setPromo(promo2);
		etud5.setPromo(promo2);
		
		//Initialsiation des equipements
		Map <Equipement,Integer> equipements1 = new HashMap<Equipement,Integer>();
		equipements1.put(Equipement.VIDEOPROJECTEUR,1);
		Map <Equipement,Integer> equipements2 = new HashMap<Equipement,Integer>();
		equipements2.put(Equipement.VIDEOPROJECTEUR,2);
		Map <Equipement,Integer> equipements3 = new HashMap<Equipement,Integer>();
		equipements3.put(Equipement.LAVABO, 2);
		
		//Initialisation des salles
		Salle salle1 = new Salle(2, "L207");
		Salle salle2 = new Salle(5, "M403", equipements1);

		//Initialisation des notes
		Note note1 = new Note(etud1, Matiere.MATHEMATIQUES, 15, 1);
		Note note2 = new Note(etud1, Matiere.MATHEMATIQUES, 16, 1);
		Note note3 = new Note(etud1, Matiere.FRANCAIS, 8, 2);
		Note note4 = new Note(etud1, Matiere.FRANCAIS, 16, 1);
		Note note5= new Note(etud1, Matiere.BIOLOGIE, 9, 2);
		Note note6 = new Note(etud2, Matiere.MATHEMATIQUES, 8, 1);
		Note note7 = new Note(etud2, Matiere.FRANCAIS, 17, 2);
		Note note8 = new Note(etud3, Matiere.MATHEMATIQUES, 10, 1);
		Note note9 = new Note(etud3, Matiere.FRANCAIS, 12, 2);
		Note note10 = new Note(etud4, Matiere.BIOLOGIE, 4, 1);
		Note note11 = new Note(etud4, Matiere.CHIMIE, 11, 1);
		Note note12 = new Note(etud5, Matiere.BIOLOGIE, 15, 1);
		Note note13 = new Note(etud5, Matiere.CHIMIE, 15, 1);
		
		//attribution des notes
		prof1.noter(note1);
		prof1.noter(note6);
		prof1.noter(note11);
		prof2.noter(note4);
		prof2.noter(note3);
		prof2.noter(note13);
		prof2.noter(note9);
		prof5.noter(note12);
		prof1.noter(note2);
		prof2.noter(note7);
		prof5.noter(note10);
		prof1.noter(note8);
		prof5.noter(note5);
		
		System.out.println("Exemple :\n\n" + etud1.toString() + "\n\n** L'étudiant visualise son emploi du temps. **");
		etud1.visualiserEmploiDuTemps();
		
		System.out.println("\n\n** L'étudiant visualise son bulletin. **");
//		etud1.visualiserBulletin();
		
		etud1.editerBulletin();
		System.out.println("\n** L'étudiant fait éditer le bulletin.\nLe bulletin est édité et sauvegardé dans le workspace. **");
		
		PersonnelAdministratif pa = new PersonnelAdministratif("Morand","Myriam");
		Calendar calCours = Calendar.getInstance();
		String dDebut = "01-09-2019-08:00:00";
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		
		
		try {
			Date dateDebut = sdf.parse(dDebut);
			calCours.setTime(dateDebut);
			System.out.println("\n** Un membre du personnel administratif choisit de saisir\nun cours de mathématiques pour les IT1, aujourd'hui,\nen M404, avec Benoit Costes."
					+ "\nIl demande une salle qui comporte un vidéo projecteur. **");
			pa.ajouterCours(calCours, salle2, prof1, promo1, Matiere.MATHEMATIQUES,equipements1);
			
			//"Place insuffisante dans la salle."
			System.out.println("\n** Un membre du personnel administratif choisit de saisir un cours de Français pour les IT1, aujourd'hui, en L207, avec Jean-François Hangouët."
					+ "\nIl demande une salle qui comporte deux vidéo projecteurs. **\n");
			pa.ajouterCours(calCours, salle1, prof2, promo1, Matiere.FRANCAIS,equipements2);
			
			//"Le prof n'enseigne pas cette matière."
			System.out.println("\n** Un membre du personnel administratif choisit de saisir un cours de Français pour les IT2, aujourd'hui, en L207, avec Oussama Ennafi."
					+ "\nIl demande une salle qui comporte deux vidéo projecteurs. **\n");
			pa.ajouterCours(calCours, salle1, prof3, promo2, Matiere.FRANCAIS,equipements2);
			
			System.out.println("\n** Un membre du personnel administratif choisit de saisir un cours de Français pour les IT2, aujourd'hui, en M403, avec Jean-François Hangouët."
					+ "\nIl demande une salle qui comporte deux vidéo projecteurs. **\n");
			//"L'equipement : VIDEOPROJECTEUR n'est pas en quantité suffisante"
			pa.ajouterCours(calCours, salle2, prof2, promo2, Matiere.FRANCAIS,equipements2);
			
			System.out.println("\n** Un membre du personnel administratif choisit de saisir un cours de Français pour les IT2, aujourd'hui, en L207, avec Jean-François Hangouët."
					+ "\nIl demande une salle qui comporte deux lavabos. **\n");
			//"LAVABO n'est pas disponible dans cette salle"
			pa.ajouterCours(calCours, salle1, prof2, promo2, Matiere.FRANCAIS,equipements3);
			

			System.out.println("\n** Le membre du personnel administartif souhaite visualiser l'emploi du temps de Benoit Costes **\n");
			pa.visualiserEmploiDuTempsProf(prof1);
			
			System.out.println("\n** Le membre du personnel administartif souhaite visualiser l'emploi du temps des IT1 **\n");
			pa.visualiserEmploiDuTempsPromo(promo1);
			
			System.out.println("\n** Un étudiant IT1 souhaite visualiser son emploi du temps **\n");
			etud3.visualiserEmploiDuTemps();

		} catch (ParseException e) {
			e.printStackTrace();		
		}
	}
	
}
