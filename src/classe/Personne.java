package classe;

/**
 * Classe representant une personne.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public abstract class Personne implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Attribut qui correspond au nom de la personne.
	 */
	private String nom;
	
	/**
	 * Attribut qui correspond au prenom de la personne.
	 */
	private String prenom;
	
	/**
	 * Attribut qui correspond au mot de passe de la personne
	 */
	private String mdp;

	/**
	 * constructeur de personne avec son nom et son prenom.
	 * @param nom
	 * @param prenom
	 */
	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
		this.mdp="";
	}
	
	/**
	 * Constructeur vide de personne
	 */
	public Personne() {
		super();
		this.nom="";
		this.prenom="";
		this.mdp="";
	}
	
	/**
	 * Constructeur complet de personne avec nom prenom et mot de passe
	 */
	public Personne(String nom, String prenom, String mdp) {
		super();
		this.nom=nom;
		this.prenom=prenom;
		this.mdp=mdp;
	}
	
	/**
	 * getter de nom
	 * @return le nom de la personne
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * getter de prenom
	 * @return le prenom de la personne
	 */
	public String getPrenom() {
		return this.prenom;
	}
	
	/**
	 * getter de mdp
	 * @return le mot de passe de la personne
	 */
	public String getMdp() {
		return this.mdp;
	}
	
	/**
	 * setter de nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * setter de prenom
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	/**
	 * setter de mdp
	 * @param mdp
	 */
	public void setMdp(String mdp) {
		this.mdp=mdp;
	}
	
	/**
	 * Methode qui redefinit toString().
	 * @return le String qui donne le nom et le prenom de la personne.
	 */
	@Override
	public String toString() {
		return this.getNom() + " " + this.getPrenom();
	}
}
