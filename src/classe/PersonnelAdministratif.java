package classe;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


/**
 * Classe representant le personnel administratif.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class PersonnelAdministratif extends Personne implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	/**
	 * Constructeur avec le nom et le prenom du personnel administratif
	 * @param nom
	 * @param prenom
	 */
	public PersonnelAdministratif(String nom, String prenom) {
		super(nom, prenom);
	}
	
	/**
	 * Constructeur complet avec le nom le prenom et le mot de passe du personnel administratif
	 * @param nom
	 * @param prenom
	 * @param mdp
	 */
	public PersonnelAdministratif(String nom,String prenom,String mdp) {
		super(nom,prenom,mdp);
	}

	/**
	 * Constructeur vide du personnel administratif
	 *
	 */
	public PersonnelAdministratif() {
		super();
	}
	
	/**
	 * methode permettant de visualiser l emploi du temps d un professeur
	 * @param prof
	 * @return le String qui contient l emploi du temps du professeur
	 */
	public String visualiserEmploiDuTempsProf(Professeur prof) {
		Calendrier cal = prof.getCalendrier();
		System.out.println("Emploi du temps du prof : " + prof.getPrenom() +" "+
				prof.getNom()+"\n[ "+ prof.getMatiere() + " ]\n");
		Calendar today = Calendar.getInstance();
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(today)<0) {
			c++;
		}
		String e ="\tAujourd'hui\n\n";
		System.out.println("\tAujourd'hui\n\n");
		for (int i = c; i<cal.getEmploiDuTemps().size() && i<c+70;i++) {
			String s ="";
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\n\t" + strDate.substring(0,10) + "\n\n";
			}
			s+= strDate.substring(11) + "\t";
			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+= cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getPromo().getId() + " , "
						+ cal.getEmploiDuTemps().get(i).getCours().getMatiere();
			}
			else {
				s+="--";
			}
			e+= s+"\n";
			System.out.println(s);
		}
		return(e);
	}

	/**
	 * Methode permettant de visualiser l emploi du temps d une promotion
	 * @param promo
	 * @return le String qui contient l emploi du temps de la promotion
	 */
	public String visualiserEmploiDuTempsPromo(Promotion promo) {
		Calendrier cal = promo.getCalendrier();
		System.out.println("Emploi du temps de la promo : " + promo.getId() + "\n");
		Calendar today = Calendar.getInstance();
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(today)<0) {
			c++;
			//on recupere l indice qui correspond a celui de la date d aujourd hui
			//dans le calendrier de la promotion pour pouvoir commencer
			//l affichage de l emploi du temps qu a partir d aujourd hui.
		}
		String e="\tAujourd'hui\n\n";
		System.out.println("\tAujourd'hui\n\n");
		for (int i = c; i<cal.getEmploiDuTemps().size() && i<c+70;i++) {
			String s ="";
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\n\t" + strDate.substring(0,10) + "\n\n";
			}
			s+= strDate.substring(11) + "\t";

			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+=cal.getEmploiDuTemps().get(i).getCours().getSalle().getNom()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getPrenom() + " " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getNom()+ " , "
						+ cal.getEmploiDuTemps().get(i).getCours().getMatiere();
			}
			else {
				s+="--";
			}
			e+=s+"\n";
			System.out.println(s);
		}
		return(e);

	}


	/**
	 * Methode permettant de visualiser l emploi du temps d une salle
	 * @param promo
	 * @return le String qui contient l emploi du temps de la salle
	 */
	public String visualiserEmploiDuTempsSalle(Salle salle) {
		Calendrier cal = salle.getCalendrier();
		System.out.println("Emploi du temps de la Salle : " + salle.getNom() + "\n");
		Calendar today = Calendar.getInstance();
		int c=0;
		while(c<cal.getEmploiDuTemps().size() && (cal.getEmploiDuTemps().get(c).getDateDebut()).compareTo(today)<0) {
			c++;
			//on recupere l indice qui correspond a celui de la date d aujourd hui
			//dans le calendrier de la salle pour pouvoir commencer
			//l affichage de l emploi du temps qu a partir d aujourd hui.
		}
		String e="\tAujourd'hui\n\n";
		System.out.println("\tAujourd'hui\n\n");
		for (int i = c; i<cal.getEmploiDuTemps().size() && i<c+70;i++) {
			String s ="";
			DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
			String strDate = dateFormat2.format(cal.getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (cal.getEmploiDuTemps().get(i).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
				s+="\n\t" + strDate.substring(0,10) + "\n\n";
			}
			s+= strDate.substring(11) + "\t";

			if (cal.getEmploiDuTemps().get(i).getCours()!=null) {
				s+=cal.getEmploiDuTemps().get(i).getCours().getPromo()+" , " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getPrenom() + " " +
						cal.getEmploiDuTemps().get(i).getCours().getProf().getNom() +" , "
						+ cal.getEmploiDuTemps().get(i).getCours().getMatiere();
			}
			else {
				s+="--";
			}
			e+=s+"\n";
			System.out.println(s);
		}
		return e;
	}

	/**
	 * Methode permettant de connaitre les dates ou il y a deja cours avec une salle particulière,
	 * un professeur particulier et une promo particulière pour connaitre les creneaux supprimables
	 * pour par la suite faire des suppression de cours.
	 * @param salle
	 * @param prof
	 * @param promo
	 * @return la liste des dates ou il y a deja cours sous forme de String
	 */
	public List<String> datesSupprimables(Salle salle, Professeur prof, Promotion promo) {
		List<Creneau> listCoursProf = new ArrayList<Creneau>();
		listCoursProf = prof.getCalendrier().getEmploiDuTemps();
		List<String> dates = new ArrayList<String>();
		Calendar today = Calendar.getInstance();
		int q =0;
		//on regarde l indice a partir duquel on est à la date actuelle
		//sachant que les salles, les professeurs et les promotions ont un
		//calendrier de la meme taille
		while((q<listCoursProf.size()) && (listCoursProf.get(q).getDateDebut().compareTo(today)<0)) {
			q++;
		}
		for (int i=q; i<q+50;i++) {
			if (prof.getCalendrier().getEmploiDuTemps().get(i).equals(promo.getCalendrier().getEmploiDuTemps().get(i)) &&
					salle.getCalendrier().getEmploiDuTemps().get(i).equals(promo.getCalendrier().getEmploiDuTemps().get(i))) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
				String strDate = dateFormat.format(prof.getCalendrier().getEmploiDuTemps().get(i).getDateDebut().getTime());
				dates.add(strDate);
			}
		}
		return dates;
	}

	/**
	 * Methode permettant de connaitre les dates disponibles pour creer par la suite des cours.
	 * Ces dates doivent etre libres pour la salle, le professeur et la promotion.
	 * @param calendar
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 * @param equipements
	 * @return la liste des dates qui sont disponibles pour ajouter un cours sous forme de String
	 */
	public List<String> datesDisponibles(Calendar calendar, Salle salle, Professeur prof,
			Promotion promo, Matiere matiere,
			Map<Equipement, Integer> equipements) {
		List<Creneau> listCrenProf = new ArrayList<Creneau>();
		List<Creneau> listCrenPromo = new ArrayList<Creneau>();
		List<Creneau> listCrenSalle = new ArrayList<Creneau>();
		List<String> dates = new ArrayList<String>();
		if (salle.getNbPlaces()>= promo.calculEffectif()) {
			if (prof.getMatiere().equals(matiere)) {
				Set<Equipement> cles = equipements.keySet();
				Iterator<Equipement> it = cles.iterator();
				int s = 0;
				while (it.hasNext()) {
					Equipement cle =it.next();
					if (salle.getEquipements().containsKey(cle)) {
						if (equipements.get(cle)==salle.getEquipements().get(cle)) {
							s++;
						}
						else {
							System.out.println("L'equipement : "+ cle + " n'est pas en quantité suffisante.");
						}
					}
					else {
						System.out.println("L'equipement : "+ cle + " n'est pas disponible dans cette salle.");
					}
				}
				if (s==equipements.size()) {
					listCrenProf = prof.disponible(calendar);
					listCrenPromo = promo.disponible(calendar);
					listCrenSalle = salle.disponible(calendar);
					List<List<Integer>> listIntersect = new ArrayList<List<Integer>>();
					int q=0;
					int r=0;
					int t=0;
					Calendar today = Calendar.getInstance();
					//     le but des 3 while est de trouver l indice a partir duquel on est a la date actuelle dans chacun des calendriers
					//     ca reduit par la suite le temps d execution des boucles for de maniere exponentielle
					while((q<listCrenProf.size()) && (listCrenProf.get(q).getDateDebut().compareTo(today)<0)) {
						q++;
					}
					while((r<listCrenPromo.size()) && (listCrenPromo.get(r).getDateDebut().compareTo(today)<0)) {
						r++;
					}
					while((t<listCrenSalle.size()) && (listCrenSalle.get(t).getDateDebut().compareTo(today)<0)) {
						t++;
					}
					for ( int i = q; i<listCrenProf.size();i++) {
						for ( int j = r; j<listCrenPromo.size();j++) {
							for ( int k = t; k<listCrenSalle.size();k++) {
								if ((listCrenProf.get(i).getDateDebut()).compareTo(listCrenSalle.get(k).getDateDebut())==0 &&
										(listCrenProf.get(i).getDateDebut()).compareTo(listCrenPromo.get(j).getDateDebut())==0) {
									List<Integer> l = new ArrayList<Integer>();
									l.add(i);
									l.add(j);
									l.add(k);
									listIntersect.add(l);
								}
							}

						}
					}
					Calendar weekAfter = Calendar.getInstance();
					weekAfter.add(Calendar.DATE,7);
					for (int i=0;i<listIntersect.size() && listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().compareTo(weekAfter)<0;i++) {
						DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
						String strDate = dateFormat.format(listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().getTime());
						dates.add(strDate);
					}
				}
			}
		}
		return dates;
	}


	/**
	 * Methode permettant d'ajouter un cours en passant par la fenetre graphique.
	 * @param dates
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 */
	public void ajouterCoursGraphique(String dates, Salle salle, Professeur prof,
			Promotion promo, Matiere matiere) {
		Cours cours = new Cours(salle, prof, promo, matiere);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
		for(int i=0;i< prof.getCalendrier().getEmploiDuTemps().size();i++) {
			String strDate = dateFormat.format(prof.getCalendrier().getEmploiDuTemps().get(i).getDateDebut().getTime());
			if (dates.equals(strDate)){
				prof.getCalendrier().getEmploiDuTemps().get(i).setCours(cours);
				salle.getCalendrier().getEmploiDuTemps().get(i).setCours(cours);
				promo.getCalendrier().getEmploiDuTemps().get(i).setCours(cours);
			}
		}
	}


	/**
	 * Methode ayant pour but d ajouter un cours a un prof, etudiant et salle particuliere en fonction de leur dsiponibilite
	 * interraction avec l utilisateur qui choisi le creneau parmis une selection.
	 * Cette methode permet l ajout d un cours dans le terminal.
	 * @param calendar
	 * @param salle
	 * @param prof
	 * @param promo
	 * @param matiere
	 * @param nbCreneaux
	 * @param equipements
	 */
	public void ajouterCours(Calendar calendar, Salle salle, Professeur prof,
			Promotion promo, Matiere matiere,
			Map<Equipement, Integer> equipements) {
		List<Creneau> listCrenProf = new ArrayList<Creneau>();
		List<Creneau> listCrenPromo = new ArrayList<Creneau>();
		List<Creneau> listCrenSalle = new ArrayList<Creneau>();
		if (salle.getNbPlaces()>= promo.calculEffectif()) {
			if (prof.getMatiere().equals(matiere)) {
				Set<Equipement> cles = equipements.keySet();
				Iterator<Equipement> it = cles.iterator();
				int s = 0;
				while (it.hasNext()) {
					Equipement cle =it.next();
					if (salle.getEquipements().containsKey(cle)) {
						if (equipements.get(cle)==salle.getEquipements().get(cle)) {
							s++;
						}
						else {
							System.out.println("L'equipement : "+ cle + " n'est pas en quantité suffisante.");
						}
					}
					else {
						System.out.println("L'equipement : "+ cle + " n'est pas disponible dans cette salle.");
					}
				}
				if (s==equipements.size()) {
					listCrenProf = prof.disponible(calendar);
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
					listCrenPromo = promo.disponible(calendar);
					listCrenSalle = salle.disponible(calendar);
					List<List<Integer>> listIntersect = new ArrayList<List<Integer>>();
					int q=0;
					int r=0;
					int t=0;
					Calendar today = Calendar.getInstance();
					//     le but des 3 while est de trouver l indice a partir duquel on est a la date actuelle dans chacun des calendriers
					//     ca reduit par la suite le temps d execution des boucles for de maniere exponentielle
					while((q<listCrenProf.size()) && (listCrenProf.get(q).getDateDebut().compareTo(today)<0)) {
						q++;
					}
					while((r<listCrenPromo.size()) && (listCrenPromo.get(r).getDateDebut().compareTo(today)<0)) {
						r++;
					}
					while((t<listCrenSalle.size()) && (listCrenSalle.get(t).getDateDebut().compareTo(today)<0)) {
						t++;
					}
					for ( int i = q; i<listCrenProf.size();i++) {
						for ( int j = r; j<listCrenPromo.size();j++) {
							for ( int k = t; k<listCrenSalle.size();k++) {
								if ((listCrenProf.get(i).getDateDebut()).compareTo(listCrenSalle.get(k).getDateDebut())==0 &&
										(listCrenProf.get(i).getDateDebut()).compareTo(listCrenPromo.get(j).getDateDebut())==0) {
									List<Integer> l = new ArrayList<Integer>();
									l.add(i);
									l.add(j);
									l.add(k);
									listIntersect.add(l);
								}
							}

						}
					}
					System.out.println("\nLes disponibilités pour la semaine à venir sont : \n");
					Calendar weekAfter = Calendar.getInstance();
					weekAfter.add(Calendar.DATE,7);
					System.out.println("\tAujourd'hui\n");
					String s2 ="";
					for (int i=0;i<listIntersect.size() && listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().compareTo(weekAfter)<0;i++) {
						s2 ="";
						DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");  
						String strDate = dateFormat.format(listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().getTime());
						if (listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().get(Calendar.HOUR_OF_DAY)<=8) {
							s2+="\n\t" + strDate.substring(0,10) + "\n\n" + strDate.substring(11);
						}
						else {
							s2+= strDate.substring(11) + "\t";
						}
						System.out.println(s2);
					}
					Calendar cal = Calendar.getInstance();
					boolean a=true;
					String date2 = "";
					while(a) {
						Scanner sc = new Scanner(System.in);
						System.out.println("Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)");
						date2 = sc.nextLine();
						while (!(date2.length()==19 && date2.substring(2,3).equals("-") && date2.substring(5,6).equals("-") &&
								date2.substring(10,11).equals("-") && date2.substring(13,14).equals(":") && date2.substring(16,17).equals(":"))) {
							System.out.println("\nLe creneau que vous avez saisi n'existe pas.\nVeuillez recommencer:\n");
							sc = new Scanner(System.in);
							System.out.println("Quel creneau choisissez-vous ? (dd-MM-yyyy-HH:mm:ss)");
							date2 = sc.nextLine();
							a = false;
						}
						String[] date = date2.split("-",4);
						String[] horaire = date[3].split(":",3);
						int jour = Integer.parseInt(date[0]);
						int mois = Integer.parseInt(date[1]);
						int annee = Integer.parseInt(date[2]);
						int heure = Integer.parseInt(horaire[0]);
						int minute = Integer.parseInt(horaire[1]);
						int seconde = Integer.parseInt(horaire[2]);
						if (annee>2025 || annee<2019 || mois>12 || mois<0 ||
								jour>31 || jour<0 || heure<8 || heure==13 ||
								heure>18 || minute>=60 || minute<0 || seconde>=60 || seconde<0) {
							System.out.println("\nLe creneau que vous avez saisi n'existe pas.\nVeuillez recommencer\n");
							a=true;
						}
						else {
							a=false;
						}
						sc.close();
					}
					try {
						cal.setTime(sdf.parse(date2));
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					Cours cours = new Cours(salle, prof, promo, matiere);
					for (int i=0;i<listIntersect.size();i++) {
						if (listCrenProf.get(listIntersect.get(i).get(0)).getDateDebut().compareTo(cal)==0) {
							listCrenProf.get(listIntersect.get(i).get(0)).setCours(cours);
						}
						if (listCrenPromo.get(listIntersect.get(i).get(1)).getDateDebut().compareTo(cal)==0) {
							listCrenPromo.get(listIntersect.get(i).get(1)).setCours(cours);
						}
						if (listCrenSalle.get(listIntersect.get(i).get(2)).getDateDebut().compareTo(cal)==0) {
							listCrenSalle.get(listIntersect.get(i).get(2)).setCours(cours);
						}
					}    
				}
			}
			else {
				System.out.println("Le prof n'enseigne pas cette matiere.");
			}
		}
		else {
			System.out.println("Place insuffisante dans la salle.");
		}
	}



	/**
	 *Methode ayant pour but de supprimer un cours
	 * @param calendar
	 * @param promo
	 */
	public void supprimerCours(Calendar calendar,Promotion promo) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss");
		List<Creneau> cal = promo.getCalendrier().getEmploiDuTemps();
		Professeur prof = new Professeur();
		Salle salle =new Salle();
		Cours c = new Cours();
		for (int i = 0; i<cal.size();i++) {
			if (cal.get(i).getDateDebut().compareTo(calendar)==0) {
				c = cal.get(i).getCours();
				if (c!=null) {
					prof = c.getProf();
					salle = c.getSalle();
					prof.getCalendrier().getEmploiDuTemps().get(i).setCours(null);
					salle.getCalendrier().getEmploiDuTemps().get(i).setCours(null);
					cal.get(i).setCours(null);
					break;
				}
				else {

					System.out.println("Pas de cours a supprimer a la promo " + promo.getId() + " au creneau du " + sdf.format(calendar.getTime()));
					return;
				}
			}
			if (cal.get(i).getDateDebut().compareTo(calendar)>0) {
				System.out.println("Date " + sdf.format(calendar.getTime()) +  " incorrecte, pas de correspondance avec le calendrier de la promo " + promo.getId());
				break;
			}
		}
	}
}


