package classe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Classe representant une salle.
 * @author Sengeissen Enola, Van Hecke Alex, Sobreira Oriane
 *
 */
public class Salle implements IDisponible{
	
	/**
	 * Attribut qui correspond au nombre de places de la salle.
	 */
	private int nbPlaces;
	
	/**
	 * Attribut qui correspond au nom de la salle.
	 */
	private String nom;
	
	/**
	 * Attribut qui correspond aux equipements de la salle.
	 */
	private Map<Equipement,Integer> equipements;
	
	/**
	 * Attribut qui correspond au calendrier de la salle.
	 */
	private Calendrier calendrier;
	
	/** 
	 * constructeur de salle avec un nombre de places et un nom de salle.
	 * @param nbPlaces
	 * @param nom
	 */
	public Salle(int nbPlaces, String nom) {
		this.nbPlaces = nbPlaces;
		this.nom = nom;
		this.equipements=new HashMap<Equipement,Integer>();
		this.calendrier =new Calendrier();
	}
	
	
	/** 
	 * constructeur de salle avec un nombre de places, un nom de salle et des equipements.
	 * @param nbPlaces
	 * @param nom
	 * @param equipements
	 */
	public Salle(int nbPlaces, String nom, Map<Equipement, Integer> equipements) {
		this.nbPlaces = nbPlaces;
		this.nom = nom;
		this.equipements = equipements;
		this.calendrier =new Calendrier();
	}
	
	/**
	 * Constructeur vide de salle
	 */
	public Salle() {
		super();
		this.calendrier =new Calendrier();
	}


	/** 
	 * getter du nombre de places
	 * @return
	 */
	public int getNbPlaces() {
		return this.nbPlaces;
	}
	
	/**
	 * setteur de calendrier
	 * @param calendrier
	 */
	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}

	/**
	 * getter du nom de la salle
	 * @return
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * getter des equipements de la salle
	 * @return
	 */
	public Map<Equipement, Integer> getEquipements() {
		return this.equipements;
	}
	
	/**
	 * getteur du calendrier de la salle
	 * @return
	 */
	public Calendrier getCalendrier() {
		return this.calendrier;
	}

	/**
	 * setter du nombre de place
	 * @param nbPlaces
	 */
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	/**
	 * setter du nom
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * setter des equipements
	 * @param equipements
	 */
	public void setEquipements(Map<Equipement, Integer> equipements) {
		this.equipements = equipements;
	}

	/**
	 * Redefinition de la methode toString qui permet un affichage du nom de la salle
	 * lorsqu on l appelle.
	 */
	@Override
	public String toString() {
		return this.getNom();
	}
	
	/**
	 * Methode qui permet de savoir les creneaux disponibles de la salle a partir d une date donnee
	 * du type Calendar.
	 * @return la liste des creneaux disponibles de la salle.
	 */	
	@Override
	public List<Creneau> disponible(Calendar calendar) {
		List<Creneau> c =this.calendrier.getEmploiDuTemps();
		List<Creneau> l = new ArrayList<Creneau>();
		int j;
		for (j =0; j< c.size();j++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(c.get(j).getDateDebut().getTime());
			if (c.get(j).getCours()==null) {
				l.add(c.get(j));
			}
		}
	return l;
	}
	
}
